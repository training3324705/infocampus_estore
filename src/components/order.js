import React from 'react'
import AdminHeader from './Adminheader'
import { useState, useEffect } from 'react'
import axios from 'axios'

// if(localStorage.getItem("vendorid")=== null){
//     window.location.href="http://localhost:3000/#/account";
// }

export default function ManageOrder() {
    const [order, setOrder] = useState([]);

    const getorder = () => {
        axios.get("http://localhost:1234/order")
            .then(response => {
                setOrder(response.data.reverse());
            })
    }

    useEffect(() => {
        getorder();
    }, [])
  return (
    <>
        <AdminHeader/>
        <div className="container mt-5">
            {
                order.map((info, index)=>{
                    return(
        <div className="row" key={index}>
            <div className="col-lg-4 mb-5 shadow ">
                <div className="p-4">
                    <h6 className='text-center text-primary'>Customer Details</h6>
                    <p>Full Name : {info.customername}</p>
                    <p>Mobile No. : {info.mobileno}</p>
                    <p>Email Id : {info.emailid}</p>
                    <p>Address : {info.address}</p>
                </div>
            </div>

            <div className="col-lg-8">
                <h6 className='text-center text-info'>Order Details</h6>
                <div className="p-2">
                    
                    <table className="table table-bordered table-hover">
                            <thead>
                                <tr className=' text-primary'>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Price</th>
                                    <th>Photo</th>
                                    <th>Details</th>
                                </tr>
                            </thead>

                            <tbody>
                                {
                                    info.myproduct.map((product, index)=>{
                                        return(
                                            <tr key={index}>
                                                <td>{product.id}</td>
                                                <td>{product.pname}</td>
                                                <td>{product.pprice}</td>
                                                <td><img src={product.pphoto} height="50" width="80"/>
                                                </td>
                                                <td>{product.pdetails}</td>
                                            </tr>
                                        )
                                    })
                                }
                            </tbody>
                    </table>
                </div>
            </div>
        </div>
                 )
             })
        }  
    </div>
    </>
  )
}
