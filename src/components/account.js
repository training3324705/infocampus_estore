import React, { useState } from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom'
export default function Account() {
    const [vendorname, setVendorName] = useState("");
    const [vendoremail, setVendorEmail] = useState("");
    const [vendorpassword, setVendorPsw] = useState("");
    const [vendormobile, setVendorMobile] = useState("");
    const [msg, setMsg] = useState("");


    const register = ()=>{
        // alert("0k");
        var user = {
            "name": vendorname,
            "email": vendoremail,
            "password": vendorpassword,
            "mobile": vendormobile
        };
        // post function goes here
        var url = "http://localhost:1234/vendor";
        axios.post(url, user)
        .then(response=>{
            setMsg("User Updated");
            // to clear textbox
                setVendorName("");
                setVendorEmail ("");
                setVendorPsw("");
                setVendorMobile("");
        })
    }

    // for login
    const [useremail, pickUseremail] = useState("");
    const [userpass, pickUserpass] = useState("");
    const [emailmsg, updateEmailmsg] = useState("");

    const goLogin = ()=>{
        var loginstatus = false;
        updateEmailmsg("please wait Processing");
        var url = "http://localhost:1234/vendor";
        axios.get(url).then(response=>{
            let alluser = response.data;
            for(var i=0; i<alluser.length; i++){
                if(alluser[i].email === useremail && alluser[i].password === userpass){
                        updateEmailmsg("success: Redirecting...");
                        loginstatus = true;
                        localStorage.setItem("name", alluser[i].name);
                        localStorage.setItem("vendorid", alluser[i].id);
                        localStorage.setItem("mobile", alluser[i].mobile);
                        window.location.href="http://localhost:3000/#/myorder";
                        break;
                }
            }
            if(loginstatus===false){
                updateEmailmsg("Failed: Invalid or Not Exists Acc")
            }
        })
    }

    return (
        <div className='container mt-4'>
            <div className="row">
                <div className="col-lg-12 text-center mb-4 text-primary fw-bold">
                    <h3>Vendor Acccount</h3>
                    <p><Link to="../" className="text-decoration-none"><button className='btn btn-primary btn-sm'>Back To Home</button></Link></p>
                </div>
                <div className="col-lg-2"></div>
                <div className="col-lg-3">
                    <div className="card">
                        <div className="card-header m-2">
                            <h2>Login</h2>
                            <p>{emailmsg}</p>
                        </div>
                            {/* <hr /> */}
                        <div className="card-body">
                            <div className="mb-2">
                                <i className="fa fa-lock p-1"></i>
                                <label htmlFor="email" >Email Id</label>
                                <input type="email" className='form-control' required='required' onChange={obj=>pickUseremail(obj.target.value)}/>
                            </div>
                            <div className="mb-2">
                                <i className="fa fa-lock p-1"></i>
                                <label htmlFor="psw" >Password</label>
                                <input type="password" className='form-control' onChange={obj=>pickUserpass(obj.target.value)}/>
                            </div>
                        </div>
                        <div className="card-footer text-center ">
                            <button className='btn btn-primary mb-2' onClick={goLogin}>Login</button>
                        </div>
                    </div>
                </div>

                <div className="col-lg-1"></div>
                <div className="col-lg-4">
                    <div className="card">
                        <h5 className='text-center text-success'>{msg}</h5>
                        <div className="card-header m-2"><h2>Register</h2>
                        <p>Please Fill This Form To Create An Account!</p>
                        </div>
                        {/* <hr /> */}
                        <div className="card-body">
                            <div className="mb-2">
                        <i className="fa fa-user p-1"></i>
                                <label htmlFor="name">Name</label>
                                <input type="text" className='form-control' onChange={e=>setVendorName(e.target.value)} value={vendorname} />
                            </div>
                            <div className="mb-2">
                            <i className="fa fa-check p-1"></i>
                                <label htmlFor="mno">Mobile No.</label>
                                <input type="number" className='form-control' onChange={e=>setVendorMobile(e.target.value)} value={vendormobile} />
                            </div>
                            <div className="mb-2">
                            <i className="fa fa-lock p-1"></i>
                                <label htmlFor="email">Email Id</label>
                                <input type="email" className='form-control' onChange={e=>setVendorEmail(e.target.value)} value={vendoremail}/>
                            </div>
                            <div className="mb-3">
                                <i className="fa fa-lock p-1"></i>
                                <label htmlFor="psw">Password</label>
                                <input type="password" className='form-control' onChange={e=>setVendorPsw(e.target.value)} value={vendorpassword} />
                            </div>
                            <div className="mb-2">
                            <label className="form-check-label fw-bold"><input type="checkbox" className='m-1' required="required"/>I Accept The Terms of Use &amp; Privacy Policy</label>
                            </div>
                        </div>
                        <div className="card-footer text-center ">
                            <button className='btn btn-primary mb-2' onClick={register}>Register</button>
                            </div>
                        </div>
                {/* <form>
                    <h2>Register</h2>
                    <p>Please fill in this form to create an account!</p>
                    <hr />
                    <div className="form-group">
                        <div className="input-group">
                        <div className="input-group-prepend">
                        <span className="input-group-text">
                        <span className="fa fa-user p-1"></span>
                        </span>
                        </div>
                        <input type="text" className="form-control mb-4" name="username" placeholder="Username" required="required" onChange={e=>setVendorName(e.target.value)} value={vendorname}/>
                        </div>
                        </div>
                        <div className="form-group">
                        <div className="input-group">
                            <div className="input-group-prepend">
                            <span className="input-group-text">
                            <i className="fa fa-paper-plane p-1"></i>
                            </span>
                            </div>
                            <input type="email" className="form-control mb-4" name="email" placeholder="Email Address" required="required" onChange={e=>setVendorEmail(e.target.value)} value={vendoremail}/>
                            </div>
                            </div>
                            <div className="form-group">
                            <div className="input-group">
                            <div className="input-group-prepend">
                            <span className="input-group-text">
                            <i className="fa fa-lock p-1"></i>
                                </span>
                                </div>
                                <input type="text" className="form-control mb-4" name="password" placeholder="Password" required="required" onChange={e=>setVendorPsw(e.target.value)} value={vendorpassword}/>
                                </div>
                                </div>
                                <div className="form-group">
                                <div className="input-group">
                                <div className="input-group-prepend">
                                <span className="input-group-text">
                                <i className="fa fa-check p-1"></i>
                                </span>
                                </div>
                                <input type="number" className="form-control mb-4" name="confirm_password" placeholder="Mobile No." required="required" onChange={e=>setVendorMobile(e.target.value)} value={vendormobile}/>
                                </div>
                                </div>
                                <div className="form-group">
                                <label className="form-check-label"><input type="checkbox" required="required"/> I accept the Terms of Use &amp; Privacy Policy</label>
                                </div>
                                <div className="form-group text-center mt-2">
                                <button  className="btn btn-primary btn-lg" onClick={register}>Register</button>
                                </div>
                            </form> */}
                        </div>
                <div className="col-lg-2"></div>
            </div>
        </div>
    )
}