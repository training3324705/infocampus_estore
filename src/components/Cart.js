import React from 'react'
import MyHeader from './Header'
import axios from 'axios'
import { useState, useEffect } from 'react'

export default function MyCart() {
    const [product, setProduct] = useState([]);

    const getproduct = () => {
        axios.get("http://localhost:1234/cart")
            .then(response => {
                setProduct(response.data);
            })
    }

    useEffect(() => {
        getproduct();
    }, [])

    const[message, setMessage] = useState();

    const delitem = (pid)=>{
        axios.delete("http://localhost:1234/cart/" + pid)
            .then(response => {
                setMessage("Item Deleted Successfully");
                getproduct();
            })
    }

    const [customer, setCustomer] = useState("");
    const [email, setEmail] = useState("");
    const [mobile, setMobile] = useState("");
    const [address, setAddress] = useState("");

    const placeorder = ()=>{
        var myorder ={
            myproduct:product,
            customername:customer,
            mobileno:mobile,
            emailid:email,
            address:address
        }
        var url = "http://localhost:1234/order";
        axios.post(url, myorder)
        .then(response=>{
            setMessage("Placed Order Succussfully");
            getproduct();
        })
    }
  return (
    <>
    <MyHeader/>
    <div className="container mt-5">
        <div className="row">
            <div className="col-lg-3">
                <div className="p-4 shadow">
                    <h4 className='text-center text-primary'>Customer Details</h4>
                    <div className="mb-3">
                        <label htmlFor="name">Customer Name</label>
                        <input type="text" value={customer} onChange={e=>setCustomer(e.target.value)} className='form-control' />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="email">Email Id</label>
                        <input type="email" value={email} onChange={e=>setEmail(e.target.value)} className='form-control' />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="number">Mobile No.</label>
                        <input type="number" value={mobile} onChange={e=>setMobile(e.target.value)} className='form-control' />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="number">Delivery Address</label>
                        <textarea className='form-control' value={address} onChange={e=>setAddress(e.target.value)}> </textarea>
                    </div>
                    <div className="mb-3 text-center">
                        <button className='btn btn-primary' onClick={placeorder}>Place Order</button>
                    </div>
                </div>
            </div>

            <div className="col-lg-9">
                <div className="p-4 shadow">
                    <h4 className='text-center text-info'>Items In Cart</h4>
                    <p className='text-center text-success'>{message}</p>
                    <table className="table table bordered shadow table-hover">
                            <thead>
                                <tr className='bg-light text-primary'>
                                    <th>Product Id</th>
                                    <th>Name</th>
                                    <th>Price</th>
                                    <th>Photo</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                {
                                    product.map((pinfo, index)=>{
                                        return(
                                            <tr>
                                                <td>{pinfo.id}</td>
                                                <td>{pinfo.pname}</td>
                                                <td>{pinfo.pprice}</td>
                                                <td><img src={pinfo.pphoto} height="50" width="70"/></td>
                                                <td>
                                                    <button className='btn btn-danger btn=sm' onClick={e=>delitem(pinfo.id)}><i className='fa fa-trash'></i>Delete</button>
                                                </td>
                                            </tr>
                                        )

                                    })
                                }
                            </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </>
  )
}
