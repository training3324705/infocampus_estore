import axios from 'axios';
import React, { useState, useEffect } from 'react'
import AdminHeader from './Adminheader'

export default function ManageProduct() {
        const [productlist, updateProduct] = useState([]);
        const getProduct = ()=>{
            axios.get("http://localhost:1234/product")
            .then(response=>{
                updateProduct(response.data)
            })
        }

    useEffect(() => {
        getProduct();
    }, [true])

    const [itemname, pickName] = useState("");
    const [itemprice, pickPrice] = useState("");
    const [itemphoto, pickPhoto] = useState("");
    const [itemdetails, pickDetails] = useState("");
    const [msg, updateMsg] = useState("");

    const save = ()=>{
        var newitem ={
            "pname": itemname,
            "pprice": itemprice,
            "pphoto": itemphoto,
            "pdetails": itemdetails
        }
        var url = "http://localhost:1234/product";
        axios.post(url, newitem)
        .then(response=>{
            updateMsg(itemname, "Saved Successfully");
            pickName("");
            pickPrice("");
            pickPhoto("");
            pickDetails("");
            getProduct();
        })
    }

    const delproduct = (id) =>{
        axios.delete("http://localhost:1234/product/"+id)
        .then(response=>{
            getProduct()
            updateMsg("Product Deleted Successfully")
        })

    }

    
  return (
    <>
        <AdminHeader/>
        <div className="container mt-5">
        <div className="row">
            <div className="col-lg-3">
                <div className="p-4 shadow">
                    <h4 className='text-center text-primary'>New Product</h4>
                    <div className="mb-3">
                        <label htmlFor="name">Product Name</label>
                        <input type="text" className='form-control' onChange={e=>pickName(e.target.value)} value={itemname} />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="email">Product Price</label>
                        <input type="email" className='form-control' onChange={e=>pickPrice(e.target.value)} value={itemprice} />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="number">Product Photo</label>
                        <input type="text" className='form-control' onChange={e=>pickPhoto(e.target.value)} value={itemphoto} />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="number">Product Details</label>
                        <textarea className='form-control' onChange={e=>pickDetails(e.target.value)} value={itemdetails}> </textarea>
                    </div>
                    <div className="mb-3 text-center">
                        <button className='btn btn-primary' onClick={save}>Save Product</button>
                    </div>
                </div>
            </div>

            <div className="col-lg-9">
                <div className="p-2 shadow">
                    <h4 className='text-center text-info'>{productlist.length} Manage Product</h4>
                    <p className='text-center text-success'>{msg}</p>
                    <table className="table table bordered shadow table-hover">
                            <thead>
                                <tr className='bg-light text-primary'>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Price</th>
                                    <th>Photo</th>
                                    <th>Details</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                {
                                    productlist.map((info, index)=>{
                                        return(
                                            <tr key={index}>
                                                <td>{index}</td>
                                                <td>{info.pname}</td>
                                                <td>{info.pprice}</td>
                                                <td><img src={info.pphoto} height="50" width="70"/></td>
                                                <td>{info.pdetails}</td>
                                                <td ><button className='btn btn-danger btn-sm' onClick={e=>delproduct(info.id)}><i className='fa fa-trash'></i>Delete</button></td>
                                            </tr>
                                        )
                                    })
                                }
                            </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </>
  )
}
