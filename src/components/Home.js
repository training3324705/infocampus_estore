import React from 'react'
import MyHeader from './Header'
import axios from 'axios'
import { useState, useEffect } from 'react'

export default function Home() {
    const [product, setProduct] = useState([]);

    // bringing data from json
    const getproduct = () => {
        axios.get("http://localhost:1234/product")
            .then(response => {
                setProduct(response.data);
            })
    }
    // calling data on load
    useEffect(() => {
        getproduct();
    }, [])

    const[message, setMessage] = useState();

    // automatically adds item to cart
    const addtocart = (info)=>{
        setMessage("Please Wait Adding To Cart...");
        var url = "http://localhost:1234/cart";
        axios.post(url, info)
        .then(response=>{
            setMessage(info.pname + " Added In Cart Successfully!")
        })
    }
    return (
        <>
            <MyHeader />
            <div className="container mt-5">
                <div className="row">
                    <p className='col-lg-12 text-center text-success'>{message}</p>
                    {
                        product.map((info, index) => {
                            return (
                                <div className="col-md-3 mb-4" key={index}>
                                    <div className="bg-light p-3 rounded h-100 text-center" >
                                        <h5>{ info.pname }</h5>
                                            <img src={info.pphoto} alt="apple" className='img-fluid rounded mt-2 mb-2' />
                                            <p>{info.pprice}</p>
                                            <p>{info.pdetails}</p>
                                            <p>{info.prating}</p>
                                            <button className='btn btn-success btn-sm'onClick={e=>addtocart(info)}>
                                                <i className='fa fa-shopping-cart' ></i> Add To Cart</button>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div >
            </div >
    </>
    )
}
